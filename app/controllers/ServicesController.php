<?php 
    namespace App\Controllers;
    use Core\Controller;
    use Core\Session;

    class ServicesController extends Controller {
        
        /********************
         * Call the extended controller construct to 
         * instatiate the view object
         */
        public function __construct($controller, $action) {
            parent::__construct($controller, $action);

            $this->loadModel('Admin');
        }

        public function indexAction() {
            
            $this->view->render('services/service-list');
        }
        
        public function listAction() {
            
            $this->view->render('services/service-list');
        }

        public function detailAction() {
            
            $this->view->render('services/services-detail');
        }
    }
?>