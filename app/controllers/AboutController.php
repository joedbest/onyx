<?php 
    namespace App\Controllers;
    use Core\Controller;
    use Core\Session;

    class AboutController extends Controller {
        
        /********************
         * Call the extended controller construct to 
         * instatiate the view object
         */
        public function __construct($controller, $action) {
            parent::__construct($controller, $action);

            $this->loadModel('Admin');
        }

        public function indexAction() {
            
            $this->view->render('about/aboutcompany');
        }
        
        public function aboutcompanyAction() {
            
            $this->view->render('about/aboutcompany');
        }
        public function philosophyAction() {
            
            $this->view->render('about/philosophy');
        }

    }
?>