
<?php $this->start('body') ?>

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' 
          style="background-image: url(<?php echo PROOT ?>public/images/bg/02.jpg); height: 250px;">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>services</h1>
          <p>We know the secret of your success</p>
        </div>
     </div>
   </div>
  </div>
</section>


<section>
  The service detail will be here
</section>

 <!--=================================
 service-->

<section class="service white-bg page-section-ptb">
  <div class="container">
    <div class="row justify-content-center">
       <div class="col-lg-8">
          <div class="section-title text-center">
            <h2 class="title-effect"> Other services </h2>
          </div>
      </div>
    </div>
    <div class="row">
         <div class="col-lg-4 col-md-4">
            <div class="mb-30">
              <h1>01<span class="theme-color">.</span></h1>
              <h3>We have magic</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>I truly believe Augustine’s words are true and if you look at history you know it is true. There are many people in the world with amazing. </p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-4 col-md-4">
            <div class="mb-30">
              <h1>02<span class="theme-color">.</span></h1>
              <h3>We're Friendly</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>The first thing to remember about success is that it is a process – nothing more, nothing less. There is really no magic to it and it’s not.</p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-4 col-md-4">
            <div class="text-left">
              <h1>03<span class="theme-color">.</span></h1>
              <h3>We’re award winner</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>There are basically six key areas to higher achievement. Some people will tell you there are four while others may tell you there are eight. </p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
      </div>
   </div>
 </section>

 <!--=================================
 service-->


<!--=================================
action box- -->

<section class="action-box theme-bg full-width">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
       <div class="action-box-text">
        <h3><strong> Webster: </strong> The most powerful template ever on the market</h3>
        <p>Create tailor-cut websites with the exclusive multi-purpose responsive template along with powerful features.</p>
      </div>
      <div class="action-box-button">
        <a class="button button-border white" href="#">
          <span>Purchase Now</span>
          <i class="fa fa-download"></i>
       </a> 
     </div>
    </div>
  </div>
 </div>
</section>
 
<!--=================================
action box- -->
 
 
<?php $this->end() ?>
