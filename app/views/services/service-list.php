<?php $this->setSiteTitle('Onyxdatasystems - Service lists') ?>

<?php $this->start('body') ?>

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' 
          style="background-image: url(<?php echo PROOT ?>public/images/bg/serv1.jpg); height: 250px;">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      </div>
    </div>
  </div>
</section>


<section class="service white-bg page-section-ptb">
  <div class="container">
    <div class="row justify-content-center">
       <div class="col-lg-8">
            <div class="section-title text-center">
            <h2 class="title-effect">Provide excellent service</h2>
            <p>We are dedicated to providing you with the best experience possible.</p>
          </div>
      </div>
    </div>
    <div class="row">
         <div class="col-lg-4 col-md-4">
            <div class="mb-30">
              <h1>01<span class="theme-color">.</span></h1>
              <h3>Website Development and Hosting</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>I truly believe Augustine’s words are true and if you look at history you know it is true. There are many people in the world with amazing. </p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-4 col-md-4">
            <div class="mb-30">
              <h1>02<span class="theme-color">.</span></h1>
              <h3>Database Management System</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>The first thing to remember about success is that it is a process – nothing more, nothing less. There is really no magic to it and it’s not.</p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-4 col-md-4">
            <div class="text-left">
              <h1>03<span class="theme-color">.</span></h1>
              <h3>Accounting System Software</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>There are basically six key areas to higher achievement. Some people will tell you there are four while others may tell you there are eight. </p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-4 col-md-4">
            <div class="text-left">
              <h1>04<span class="theme-color">.</span></h1>
              <h3>Employee's Payroll System Software</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>There are basically six key areas to higher achievement. Some people will tell you there are four while others may tell you there are eight. </p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-4 col-md-4">
            <div class="text-left">
              <h1>05<span class="theme-color">.</span></h1>
              <h3>Hospital Management System</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>There are basically six key areas to higher achievement. Some people will tell you there are four while others may tell you there are eight. </p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
         <div class="col-lg-4 col-md-4">
            <div class="text-left">
              <h1>06<span class="theme-color">.</span></h1>
              <h3>Inventory/Stock Management System</h3>
              <img class="img-fluid full-width" src="<?php echo PROOT ?>public/images/about/01.jpg" alt="">
              <p>There are basically six key areas to higher achievement. Some people will tell you there are four while others may tell you there are eight. </p>
              <a class="button icon-color" href="#">Read More<i class="fa fa-angle-right"></i></a>
            </div>
         </div>
      </div>
   </div>
 </section>


 <?php $this->end() ?>