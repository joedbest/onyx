<header id="header" class="header default">
  <div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 xs-mb-10">
        <div class="topbar-call text-center text-md-left">
          <ul>
            <li><i class="fa fa-envelope-o theme-color"></i> info@onyxdatasystems.com</li>
             <li><i class="fa fa-phone"></i> <a href="tel:08054511357"> <span>08054511357 </span> </a> </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="topbar-social text-center text-md-right">
          <ul>
            <li><a href="#"><span class="ti-facebook"></span></a></li>
            <li><a href="#"><span class="ti-instagram"></span></a></li>
            <li><a href="#"><span class="ti-google"></span></a></li>
            <li><a href="#"><span class="ti-twitter"></span></a></li>
            <li><a href="#"><span class="ti-linkedin"></span></a></li>
            <li><a href="#"><span class="ti-dribbble"></span></a></li>
          </ul>
        </div>
      </div>
     </div>
  </div>
</div>

<!--=================================
 mega menu -->

<div class="menu">  
  <!-- menu start -->
   <nav id="menu" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items">
     <div class="container"> 
      <div class="row"> 
       <div class="col-lg-12 col-md-12"> 
        <!-- menu logo -->
        <ul class="menu-logo">
            <li>
                <a href="<?php echo PROOT ?>">
                  <img id="logo_img" style="height: 100px; width: 200px;" 
                      src="<?php echo PROOT ?>public/images/logo.png" alt="logo"> 
                </a>
            </li>
        </ul>
        <!-- menu links -->
        <div class="menu-bar">
         <ul class="menu-links">
         <li><a href="<?php echo PROOT ?>">Home </a>
                           </li>
            <li><a href="javascript:void(0)">About <i class="fa fa-angle-down fa-indicator"></i></a>
                 <!-- drop down multilevel  -->
                <ul class="drop-down-multilevel">
                    
                    <li><a href="<?php echo PROOT ?>about/aboutcompany">About The Company</a> </li>
                    <li><a href="about.php?sid=2">Who We Are </a></li>
                    <li><a href="<?php echo PROOT ?>about/philosophy">Company Philosophy </a></li>
                </ul>
            </li>
            <li><a href="javascript:void(0)">Services <i class="fa fa-angle-down fa-indicator"></i></a>
                <!-- drop down full width -->
                <div class="drop-down grid-col-12">
                    <!--grid row-->
                    <div class="grid-row">
                        <!--grid column 3-->
                        <div class="grid-col-6">
                          <ul>
                            <li><a href="<?php echo PROOT ?>services/detail">Website Development and Hosting</a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Accounting System Software</a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Hospital Management System </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Hotel Management System </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Hotel Reservation System </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Airline Reservation System </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">E-Learning System</a></li>
                          </ul>
                        </div>
                        <div class="grid-col-6">
                          <ul>
                            <li><a href="<?php echo PROOT ?>services/list">Database Management System </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Payroll System </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Inventory/Stock Management System </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Banking System Software </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Mobile Tracking System </a></li>
                            <li><a href="<?php echo PROOT ?>services/list">Advanced Security System </a></li>
                          </ul>
                        </div>
                        
                    </div>
                </div>
            </li>
            <li><a href="blog.php">blog </a>            
            <li><a href="javascript:void(0)">Our Products <i class="fa fa-angle-down fa-indicator"></i></a>
            <!-- drop down multilevel  -->
              <div class="drop-down grid-col-5 menu-offset-4">
              <!--grid row-->
               <div class="grid-row">
                 <!--grid column 3-->
                  <div class="grid-col-6">
                   <ul>
                     <li><a href="shop-home-01.html">Car GPS tracker </a></li>
                     <li><a href="shop-home-02.html">Flash Drives</a></li>
                   </ul>
                  </div>
                  <div class="grid-col-6">
                   <ul>
                     <li><a href="shop-grid.html">Mouse Pads</a></li>
                     <li><a href="shop-listing.html">Hard Disks</a></li>
                   </ul>
                  </div>
                 </div>
                </div>
             </li>
            <li><a href="<?php echo PROOT ?>contact"> Contact Us </a>
              </li>
        </ul>
        <div class="search-cart">
          <div class="search">
            <a class="search-btn not_click" href="javascript:void(0);"></a>
              <div class="search-box not-click">
                 <form action="search.html" method="get">
                  <input type="text"  class="not-click form-control" name="search" placeholder="Search.." value="" >
                  <button class="search-button" type="submit"> <i class="fa fa-search not-click"></i></button>
                </form>
           </div>
          </div>
          <div class="shpping-cart">
           <a class="cart-btn" href="#"> <i class="fa fa-shopping-cart icon"></i> <strong class="item">2</strong></a>
            <div class="cart">
              <div class="cart-title">
                 <h6 class="uppercase mb-0">Shopping cart</h6>
              </div>
              <div class="cart-item">
                <div class="cart-image">
                  <img class="img-fluid" src="<?php echo PROOT ?>public/images/shop/01.jpg" alt="">
                </div>
                <div class="cart-name clearfix">
                  <a href="#">Product name <strong>x2</strong> </a>
                  <div class="cart-price">
                    <del>$24.99</del> <ins>$12.49</ins>
                 </div>
                </div>
                <div class="cart-close">
                    <a href="#"> <i class="fa fa-times-circle"></i> </a>
                 </div>
              </div>
              <div class="cart-item">
                <div class="cart-image">
                  <img class="img-fluid" src="<?php echo PROOT ?>public/images/shop/01.jpg" alt="">
                </div>
                <div class="cart-name clearfix">
                  <a href="#">Product name <strong>x2</strong></a>
                  <div class="cart-price">
                    <del>$24.99</del> <ins>$12.49</ins>
                 </div>
                </div>
                 <div class="cart-close">
                    <a href="#"> <i class="fa fa-times-circle"></i> </a>
                 </div>
              </div>
              <div class="cart-total">
                <h6 class="mb-15"> Total: $104.00</h6>
                <a class="button" href="shop-shopping-cart.html">View Cart</a>
                <a class="button black" href="shop-checkout.html">Checkout</a>
              </div>
            </div>
          </div>
        </div>
        </div>
       </div>
      </div>
     </div>
    </section>
   </nav>
  <!-- menu end -->
 </div>
</header>