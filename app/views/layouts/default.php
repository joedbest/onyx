<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="information technology, service provider, software provider, hardware reseller, hardware maintenance, accounting system, pos systems, hr systems, factory automation" />
<meta name="description" content="Onyx Data Systems - Nigeria Foremost Information Technology Service Provider" />
<meta name="author" content="onyxdatasystems.com" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico" />

<!-- font -->
<link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,500,500i,600,700,800,900|Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:300,400,500,600,700,800">

<!-- Plugins -->
<link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>public/css/plugins-css.css" />

<!-- revoluation -->
<link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>public/css/revolution/settings.css" media="screen" />

<!-- Typography -->
<link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>public/css/typography.css" />

<!-- Shortcodes -->
<link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>public/css/shortcodes/shortcodes.css" />

<!-- Style -->
<link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>public/css/style.css" />

<!-- Responsive -->
<link rel="stylesheet" type="text/css" href="<?php echo PROOT ?>public/css/responsive.css" /> 
  
</head>

  <body>
  
    <div class="wrapper"><!-- wrapper start -->

      <!--=================================
      preloader -->
      <div id="pre-loader">
          <img src="<?php echo PROOT ?>public/images/pre-loader/loader-01.svg" alt=""> 
      </div>
      
      <?php $this->components('header');?>

      <?= $this->content('body') ?>

      </div><!-- wrapper End -->

      <?php $this->components('footer');?>

      <div id="back-to-top"><a class="top arrow" href="#top"><i class="fa fa-angle-up"></i> <span>TOP</span></a></div>


    
    <!-- jquery -->
    <script src="<?php echo PROOT ?>public/js/jquery-3.3.1.min.js"></script>

    <!-- All plugins -->
    <script src="<?php echo PROOT ?>public/js/plugins-jquery.js"></script> 

    <!-- Plugins path -->
    <script>var plugin_path = '<?php echo PROOT ?>public/js/';</script>

    <!-- REVOLUTION JS FILES -->
    <script src="<?php echo PROOT ?>public/jquery/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.actions.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.carousel.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.migration.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.navigation.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.parallax.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo PROOT ?>public/jquery/extensions/revolution.extension.video.min.js"></script>

    <!-- revolution custom --> 
    <script src="<?php echo PROOT ?>public/jquery/revolution-custom.js"></script> 

    <!-- custom -->
    <script src="<?php echo PROOT ?>public/js/custom.js"></script>
  </body>

</html>
