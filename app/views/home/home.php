<?php $this->setSiteTitle('Onyxdatasystems - Home') ?>

<?php $this->start('body') ?>
 
<!--=================================
 banner -->
 <section class="rev-slider">
  <div id="rev_slider_267_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webster-slider-1" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.6.3 fullwidth mode -->
  <div id="rev_slider_267_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.6.3">
<ul>  <!-- SLIDE  -->
    <li data-index="rs-755" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default"  data-thumb="revolution/assets/slider-01/70x70_1a353-01.jpg"  data-rotate="0,0,0,0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
        <img src="<?php PROOT ?>public/images/indeximg.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
    <!-- LAYERS -->

    <!-- LAYER NR. 1 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-755-layer-1" 
       data-x="68" 
       data-y="center" data-voffset="-30" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 5; white-space: nowrap; font-size: 40px; line-height: 40px; font-weight: 200; color: rgba(255,255,255,1); font-family:Montserrat ;">Welcome to the world  of <br/>extraordinary Software Solutions </div>

    <!-- LAYER NR. 2 -->
    <div class="tp-caption   tp-resizeme  rev-color" 
       id="slide-755-layer-2" 
       data-x="right" data-hoffset="60" 
       data-y="center" data-voffset="-57" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 6; white-space: nowrap; font-size: 150px; line-height: 150px; font-weight: 600; color: #84ba3f; font-family:Dosis;">OnyxDS </div>

    <!-- LAYER NR. 3 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-755-layer-5" 
       data-x="center" data-hoffset="-353" 
       data-y="center" data-voffset="80" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":3500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;"> Versatile  &nbsp; &nbsp;| </div>

    <!-- LAYER NR. 4 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-755-layer-7" 
       data-x="center" data-hoffset="-173" 
       data-y="center" data-voffset="80" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":4000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 10; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;"> Super Fast  &nbsp; &nbsp;| </div>

    <!-- LAYER NR. 5 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-755-layer-8" 
       data-x="center" data-hoffset="" 
       data-y="center" data-voffset="80" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":4500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 12; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;"> Creative  &nbsp; &nbsp;| </div>

    <!-- LAYER NR. 6 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-755-layer-9" 
       data-x="center" data-hoffset="159" 
       data-y="center" data-voffset="80" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":5000,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 14; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;">Modern  &nbsp; &nbsp; | </div>

    <!-- LAYER NR. 7 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-755-layer-10" 
       data-x="center" data-hoffset="302" 
       data-y="center" data-voffset="80" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":5500,"speed":1000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 16; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;">Elegant </div>

    <!-- LAYER NR. 8 -->
    <a class="tp-caption rev-btn  tp-resizeme  rev-button" 
 href="https://themeforest.net/item/webster-responsive-multipurpose-html5-template/20904293?ref=Potenzaglobalsolutions" target="_self"       id="slide-755-layer-15" 
       data-x="center" data-hoffset="" 
       data-y="center" data-voffset="160" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="button" 
      data-actions=''
      data-responsive_offset="on" 

            data-frames='[{"delay":6000,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(0,0,0);bs:solid;bw:0 0 0 0;"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[12,12,12,12]"
            data-paddingright="[35,35,35,35]"
            data-paddingbottom="[12,12,12,12]"
            data-paddingleft="[35,35,35,35]"

            style="z-index: 17; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;background-color:rgb(132,186,63);border-color:rgba(0,0,0,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Purchase Now </a>
  </li>
  <!-- SLIDE  -->
    <li data-index="rs-756" data-transition="random-static,random-premium,random" data-slotamount="default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off"  data-randomtransition="on" data-easein="default,default,default,default" data-easeout="default,default,default,default" data-masterspeed="default,default,default,default"  data-thumb="revolution/assets/slider-01/70x70_02483-02.jpg"  data-rotate="0,0,0,0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
        <img src="<?php PROOT ?>public/images/indeximg4.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
    <!-- LAYERS -->

    <!-- LAYER NR. 9 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-756-layer-2" 
       data-x="60" 
       data-y="340" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 300; color: rgba(255,255,255,1); font-family:Montserrat ;">Are You 
  </div>

    <!-- LAYER NR. 10 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-756-layer-19" 
       data-x="298" 
       data-y="340" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 6; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;"> Ready
 </div>

    <!-- LAYER NR. 11 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-756-layer-20" 
       data-x="503" 
       data-y="340" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 7; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 300; color: rgba(255,255,255,1); font-family:Montserrat ;">to
 </div>

    <!-- LAYER NR. 12 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-756-layer-3" 
       data-x="60" 
       data-y="center" data-voffset="85" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":2090,"speed":1500,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 8; white-space: nowrap; font-size: 24px; line-height: 28px; font-weight: 200; color: rgba(255,255,255,1); font-family: Montserrat ;">In today′s world, the importance of a well-executed web presence
<br/> cannot be underestimated. </div>

    <!-- LAYER NR. 13 -->
    <a class="tp-caption rev-btn  tp-resizeme  rev-button" 
 href="https://themeforest.net/item/webster-responsive-multipurpose-html5-template/20904293?ref=Potenzaglobalsolutions" target="_self"       id="slide-756-layer-12" 
       data-x="60" 
       data-y="center" data-voffset="180" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="button" 
      data-actions=''
      data-responsive_offset="on" 

            data-frames='[{"delay":3080,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(0,0,0);bc:rgb(0,0,0);bs:solid;bw:0 0 0 0;"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[12,12,12,12]"
            data-paddingright="[30,30,30,30]"
            data-paddingbottom="[12,12,12,12]"
            data-paddingleft="[30,30,30,30]"

            style="z-index: 9; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 700; color: #ffffff; font-family:Montserrat ;text-transform:uppercase;background-color:rgb(132,186,63);border-color:rgba(0,0,0,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Discover More </a>

    <!-- LAYER NR. 14 -->
    <a class="tp-caption tp-resizeme" 
 href="https://themeforest.net/item/webster-responsive-multipurpose-html5-template/20904293?ref=Potenzaglobalsolutions" target="_self"       id="slide-756-layer-13" 
       data-x="235" 
       data-y="center" data-voffset="180" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="button" 
      data-actions=''
      data-responsive_offset="on" 

            data-frames='[{"delay":3560,"speed":1000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Power0.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[10,10,10,10]"
            data-paddingright="[30,30,30,30]"
            data-paddingbottom="[10,10,10,10]"
            data-paddingleft="[30,30,30,30]"

            style="z-index: 10; white-space: nowrap; font-size: 12px; line-height: 22px; font-weight: 600; color: rgba(255,255,255,1); font-family:Montserrat ;text-transform:uppercase;background-color:rgba(0,0,0,0);border-color:rgb(255,255,255);border-style:solid;border-width:2px 2px 2px 2px;border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">purchase Now </a>

    <!-- LAYER NR. 15 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-756-layer-15" 
       data-x="60" 
       data-y="center" data-voffset="6" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 11; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 400; color: rgba(255,255,255,1); font-family:Montserrat ;">Just Change the 
 </div>

    <!-- LAYER NR. 16 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-756-layer-17" 
       data-x="565" 
       data-y="419" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 13; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">Future </div>

    <!-- LAYER NR. 17 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-756-layer-18" 
       data-x="781" 
       data-y="420" 
            data-width="['auto']"
      data-height="['auto']"
 
            data-type="text" 
      data-responsive_offset="on" 

            data-frames='[{"delay":1080,"speed":1500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['inherit','inherit','inherit','inherit']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 14; white-space: nowrap; font-size: 60px; line-height: 70px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Montserrat;">? </div>
  </li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
</div>   
</section>

<!--=================================
 banner -->
 
<!--=================================
 special-feature --> 

<section class="page-section-ptb position-relative">
   <div class="container">
   <div class="row">
      <div class="col-md-8">
        <div class="section-title">
          <h6>We're Good At</h6>
          <h2 class="title-effect">Our Services</h2>
          <p>OnyxDS softwares are easy to use and customizable User Interface (UI) elements make it most customizable template on the market. </p>
        </div>
      </div>
    </div>
     <div class="row">
        <div class="col-md-4 xs-mb-40">
          <div class="feature-box h-100">
            <div class="feature-box-content">
            <i class="fa fa-paint-brush blue"></i>
            <h4>Unique & Clean</h4>
            <p>It's always nice to browse some nice and clean websites. It is a good way to get inspiration for future projects.</p>
            </div>
            <a href="#">Read more</a>
            <div class="feature-box-img" style="background-image: url('images/about/01.jpg');"></div>
            <span class="feature-border"></span>
          </div>
        </div>
        <div class="col-md-4 xs-mb-40">
          <div class="feature-box h-100 active">
            <div class="feature-box-content">
            <i class="fa fa-arrows"></i>
            <h4>Flexible Layout</h4>
            <p>The Flexibility of our software is none to compare with, even a less computer literate person can make use of it at ease.</p>
            </div>
            <a href="#">Read more</a>
            <div class="feature-box-img" style="background-image: url('images/about/02.jpg');"></div>
            <span class="feature-border"></span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="feature-box h-100">
            <div class="feature-box-content">
            <i class="fa fa-heart"></i>
            <h4>Trendy Design</h4>
            <p>The quality of our services are designed to meet the modern day trend which we keep taps with</p>
            </div>
            <a href="#">Read more</a>
            <div class="feature-box-img" style="background-image: url('images/about/03.jpg');"></div>
            <span class="feature-border"></span>
          </div>
        </div>
       </div>
   </div> 
</section>

<!--=================================
 special-feature --> 


 <!--=================================
 awesome-features  -->

 <section class="awesome-features gray-bg page-section-ptb pos-r">
   <div class="side-background">
        <div class="col-lg-5 img-side img-left d-xs-block d-lg-block d-none">
            <div class="row page-section-pt mt-30">
              <img src="<?php PROOT ?>public/images/objects/07.png" alt="">
            </div>
        </div>
    </div>
   <div class="container">
     <div class="row justify-content-end">     
       <div class="col-lg-7">
       <div class="section-title">
          <h6>Why webster is best! </h6>
          <h2 class="title-effect">Our awesome core features</h2>
          <p>Truly ideal solutions for your business. Create a website that you gonna be proud of. </p>
        </div>
         <div class="row"> 
           <div class="col-md-6 col-sm-6">
             <div class="feature-text text-left mb-30">
             <div class="feature-icon">
              <span class="ti-desktop theme-color"></span>
              </div>
              <div class="feature-info">
                  <h5>Perfect design</h5>
                   <p>Out of box thinking and <span class="theme-color" data-toggle="tooltip" data-placement="top" title="" data-original-title="Won 10+ Awards"> Award-winning</span> web design agency. </p>
                  <a class="button icon-color" href="#">Read more <i class="fa fa-angle-right"></i></a>
               </div>
           </div>
          </div>
          <div class="col-md-6 col-sm-6">
             <div class="feature-text text-left mb-30">
             <div class="feature-icon">
              <span class="ti-headphone theme-color"></span>
              </div>
              <div class="feature-info">
                <h5>Fast Customer support</h5>
                 <p>Our Support Team will do its best to provide the best helpful answer for the issues.</p>
                <a class="button icon-color" href="#">Read more <i class="fa fa-angle-right"></i></a>
               </div>
           </div>
          </div>
         </div>
         <div class="row"> 
           <div class="col-md-6 col-sm-6">
             <div class="feature-text text-left mt-30">
             <div class="feature-icon">
              <span class="ti-panel theme-color"></span>
              </div>
              <div class="feature-info">
              <h5>Easy to Utilize</h5>
               <p>onyxdatasystems softwares have user friendly interfares</p>
              <a class="button icon-color" href="#">Read more <i class="fa fa-angle-right"></i></a>
             </div>
           </div>
          </div>
          <div class="col-md-6 col-sm-6">
             <div class="feature-text text-left mt-30">
             <div class="feature-icon">
              <span class="ti-shield theme-color"></span>
              </div>
              <div class="feature-info">
                  <h5>Powerful Performance</h5>
                   <p>High performance, super fast with top page speed & yslow ratings </p>
                  <a class="button icon-color" href="#">Read more <i class="fa fa-angle-right"></i></a>
              </div>
           </div>
          </div>
         </div>
       </div>
     </div>
   </div>
 </section>

<!--=================================
 awesome-features  -->
  
<!--=================================
about- -->

<section class="page-section-ptb">
  <div class="container">
   <div class="row">
     <div class="col-lg-6 sm-mb-30">
       <div class="owl-carousel bottom-center-dots" data-nav-dots="ture" data-smartspeed="1200" data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
         <div class="item">
            <img class="img-fluid" src="<?php PROOT ?>public/images/about/01.jpg" alt="">
          </div>
          <div class="item">
            <img class="img-fluid" src="<?php PROOT ?>public/images/about/02.jpg" alt="">
          </div>
          <div class="item">
            <img class="img-fluid" src="<?php PROOT ?>public/images/about/03.jpg" alt="">
          </div>
      </div>
     </div>
     <div class="col-lg-6">
        <div class="section-title">
        <h6>What we do?</h6>
        <h2 class="title-effect">We do things differently</h2>
        <p>We are dedicated to providing you with the best experience possible.</p>
      </div>
       <p> <span class="dropcap gray square">E</span> xclusive multi-purpose 100% responsive template with powerful features. Simple and well-structured coding, high quality and flexible layout, scalable features along with color schemes to create tailor-cut websites. With bootstrap, responsive mega menu and various layouts including incredible blog template</p>
       <div class="mt-30">
        <button type="button" class="button icon mb-10 mr-10" data-toggle="modal" data-target="#exampleModal">
             View modal popup <i class="fa fa-eye"></i>
            </button>
        <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <div class="modal-title" id="exampleModalLabel">
                       <div class="section-title mb-10">
                        <h6>EXPERTISE</h6>
                        <h2>Modal title</h2>
                        <p>We are an innovative agency. We develop and design customers around the world. Our clients are some.</p>
                      </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                   <span class="dropcap square">Y</span>ou can use model anywhere in your website consectetur adipisicing elit. At vel sed corporis delectus quo ea molestias a ab ad officiis eaque natus animi reiciendis sint beatae, dolor inventore praesentium lorem qui.
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
      <a class="button button-border icon" href="https://themeforest.net/item/webster-responsive-multipurpose-html5-template/20904293?ref=Potenzaglobalsolutions" target="_blank">
        <span>Purchase Now</span>
        <i class="fa fa-download"></i>
     </a>
    </div>
       </div>
   </div>
   <div class="row mt-80">
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="90" data-delay="0" data-type="%">
            <div class="skill-title">CSS3</div>
        </div>
     </div>
     </div>
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="75" data-delay="0" data-type="%">
            <div class="skill-title">PHP</div>
        </div>
     </div>
     </div>
     <div class="col-md-4">
       <div class="skill medium">
        <div class="skill-bar" data-percent="80" data-delay="0" data-type="%">
            <div class="skill-title">JavaScript</div>
        </div>
     </div>
     </div>
     </div>
   </div>
</section>
 
<!--=================================
about- -->

 <!--=================================
counter-->

<section class="our-service-home page-section-ptb bg-overlay-black-80 parallax" data-jarallax='{"speed": 0.6}' style="background-image: url(images/bg/06.jpg);">
 <div class="container">
  <div class="row mb-60">
      <div class="col-md-8">
        <div class="section-title">
          <h6 class="text-white">Many reasons to</h6>
          <h2 class="text-white title-effect">Why choose OnyxDS</h2>
          <p class="text-white">The lightweight template, which enables engineering the customer-oriented websites that perform to the notch.</p>
        </div>
      </div>
    </div>
  <div class="row">
     <div class="col-lg-3 col-md-6 col-sm-6 sm-mb-30">       
       <div class="counter text-white">
        <span class="icon ti-user theme-color"></span>
        <span class="timer" data-to="4905" data-speed="10000">4905</span>
        <label>ORDERED COFFEE'S</label>
      </div>
      </div>
     <div class="col-lg-3 col-md-6 col-sm-6 sm-mb-30">
     <div class="counter text-white">
        <span class="icon ti-help-alt theme-color"></span>
        <span class="timer" data-to="3750" data-speed="10000">3750</span>
        <label>QUESTIONS ANSWERED</label>
      </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6 xs-mb-30">
    <div class="counter text-white">
        <span class="icon ti-check-box theme-color"></span>
        <span class="timer" data-to="4782" data-speed="10000">4782</span>
        <label>COMPLETED PROJECTS</label>
      </div>
     </div>
      <div class="col-lg-3 col-md-6 col-sm-6">
     <div class="counter text-white">
        <span class="icon ti-face-smile theme-color"></span>
        <span class="timer" data-to="3237" data-speed="10000">3237</span>
        <label>HAPPY CLIENTS</label>
      </div>
     </div>
  </div>
 </div>
</section> 

<!--=================================
counter-->
 
 <!--=================================
 Who we are -->

<section class="white-bg page-section-ptb">
	<div class="container">
  <div class="custom-content">
		<div class="row">
  		<div class="col-md-12">
  			<div class="section-title">
          <h6 class="text-white">What we'll do</h6>
          <h2 class="text-white title-effect dark">We help to improve your business online</h2>
          <p class="text-white">We are dedicated to providing you with the best experience possible. Read below to find out why the sky's the limit when using Webster.</p>
        </div>
  	 </div>
    </div>
     <div class="row">
     <div class="col-lg-3">
          <div class="popup-video">
            <img class="img-fluid full-width" src="<?php PROOT ?>public/images/about/01.jpg" alt="">
             <div class="play-video text-center"> <a class="view-video popup-youtube" href="https://www.youtube.com/watch?v=LgvseYYhqU0"> <i class="fa fa-play"></i> </a></div>
           </div>  
        </div>
        <div class="col-lg-9">
          <h5 class="text-white">OnyxDS is one of best sSoftware development Companies you can count on</h5>
            <div class="row">
               <div class="col-md-6 col-sm-6">
                  <div class="feature-text left-icon mt-30">
                    <div class="feature-icon">
                    <span class="ti-layers-alt text-white"></span>
                  </div>
                  <div class="feature-info">
                    <h5 class="text-white">Bootstrap 4 base template</h5>
                    <p class="text-white">Consectetur dolor sit conseqt quibusdam</p>
                  </div>
                </div>
               </div>
               <div class="col-md-6 col-sm-6">
                  <div class="feature-text left-icon mt-30">
                     <div class="feature-icon">
                      <span class="ti-mouse text-white"></span>
                    </div>
                    <div class="feature-info">
                      <h5 class="text-white">Premium plugin included</h5>
                      <p class="text-white">Enim expedita sed quia nesciunt, dolor sit consectetur conseqt</p>
                    </div>
                  </div>
               </div>
            </div>
        </div>
       </div>
     </div>
  	</div>
  </section>

 <!--=================================
 Who we are -->
 
<!--=================================
 portfolio -->

<section class="portfolio-home gray-bg o-hidden">
 <div class="container-fluid p-0"> 
   <div class="row">
      <div class="col-lg-4">
        <div class="portfolio-title section-title mt-md-5">
            <h6>Super creative</h6>
            <h2 class="title-effect">Our Latest Works</h2>
            <p class="mb-20">Work on the best projects for the best clients. Our clients are some of the most forward-looking companies in the world.</p>
            <span>Webster has powerful options & tools, unlimited designs, responsive framework and amazing support. We are dedicated to providing you with the best experience possible. Purchase webster to find out why the sky's the limit when using Webster.</span>
            <a class="button mt-30" href="portfolio-classic-3-columns-fullwidth.html"> See all projects </a>
          </div>
          <div>
        </div>
      </div>
      <div class="col-lg-8">
         <div class="isotope popup-gallery columns-3 no-padding">
            <div class="grid-item">
              <div class="portfolio-item">
                 <img src="<?php PROOT ?>public/images/portfolio/small/01.jpg" alt="">
                  <div class="portfolio-overlay">
                      <h4 class="text-white"> <a href="portfolio-single-01.html">Post item image</a> </h4>
                      <span class="text-white"> <a href="#"> Photography  </a>| <a href="#">Ecommerce </a> </span>
                 </div>
                 <a class="popup portfolio-img" href="images/portfolio/small/01.jpg"><i class="fa fa-arrows-alt"></i></a>   
               </div>
             </div>
             <div class="grid-item">
              <div class="portfolio-item">
                 <img src="<?php PROOT ?>public/images/portfolio/small/02.jpg" alt="">
                  <div class="portfolio-overlay">
                      <h4 class="text-white"> <a href="portfolio-single-01.html"> Post item image</a> </h4>
                      <span class="text-white"> <a href="#"> Creative  </a>| <a href="#">Graphics </a> </span>
                 </div>
                 <a class="popup portfolio-img" href="images/portfolio/small/02.jpg"><i class="fa fa-arrows-alt"></i></a>   
               </div>
             </div>
             <div class="grid-item">
              <div class="portfolio-item">
                 <img src="<?php PROOT ?>public/images/portfolio/small/03.jpg" alt="">
                  <div class="portfolio-overlay">
                      <h4 class="text-white"> <a href="portfolio-single-01.html"> Post item image</a> </h4>
                      <span class="text-white"> <a href="#"> Design  </a>| <a href="#">Illustration </a> </span>
                 </div>
                 <a class="popup portfolio-img" href="images/portfolio/small/03.jpg"><i class="fa fa-arrows-alt"></i></a>   
               </div>
             </div>
             <div class="grid-item">
              <div class="portfolio-item">
                 <img src="<?php PROOT ?>public/images/portfolio/small/04.gif" alt="">
                  <div class="portfolio-overlay">
                      <h4 class="text-white"> <a href="portfolio-single-01.html"> Post GIF image</a> </h4>
                      <span class="text-white"> <a href="#"> Animation </a>| <a href="#">GIF </a> </span>
                 </div>
                 <a class="popup portfolio-img" href="images/portfolio/small/04.gif"><i class="fa fa-arrows-alt"></i></a>   
               </div>
             </div>
             <div class="grid-item">
              <div class="portfolio-item">
                 <img src="<?php PROOT ?>public/images/portfolio/small/05.jpg" alt="">
                  <div class="portfolio-overlay">
                      <h4 class="text-white"> <a href="portfolio-single-01.html"> Post item Youtube</a> </h4>
                      <span class="text-white"> <a href="#"> Video  </a>| <a href="#">Youtube </a> </span>
                 </div>
                 <a class="popup popup-youtube" href="https://www.youtube.com/watch?v=LgvseYYhqU0"><i class="fa fa-play"></i></a>   
               </div>
             </div>
             <div class="grid-item">
              <div class="portfolio-item">
                 <img src="<?php PROOT ?>public/images/portfolio/small/06.jpg" alt="">
                  <div class="portfolio-overlay">
                      <h4 class="text-white"> <a href="portfolio-single-01.html"> Post item image </a> </h4>
                      <span class="text-white"> <a href="#"> Photography  </a>| <a href="#">Illustration </a> </span>            
                 </div>
                 <a class="popup portfolio-img" href="images/portfolio/small/06.jpg"><i class="fa fa-arrows-alt"></i></a>   
               </div>
             </div>
         </div>
      </div>
   </div>
 </div>
</section>  
 
<!--=================================
 portfolio -->


  <!--=================================
Price table -->

<section class="page-section-ptb white-bg">
  <div class="container">
    <div class="row">
     <div class="col-md-12">
         <div class="section-title text-center mb-80">
            <h6>Choose from best of three plans </h6>
            <h2 class="title-effect">Affordable Pricing</h2>
          </div>
       </div>
    </div>
       <div class="row">
        <div class="col-md-4 ">
           <div class="pricing-table">
             <div class="pricing-top">
               <div class="pricing-title">
                 <h3 class="mb-15">Basic</h3>
                 <p>In this package dolor sit amet occaecat cupidatat non proident</p>
               </div>
               <div class="pricing-prize">
                 <h2>$49 /<span> month</span> </h2>
               </div>
               <div class="pricing-button">
                 <a class="button" href="#">Sign Up</a>
               </div>
             </div>
             <div class="pricing-content">
               <div class="pricing-table-list">
                    <ul class="list-unstyled">
                        <li> <i class="fa fa-check"></i> 05 Analytics Campaign <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span></li>
                        <li><i class="fa fa-times"></i> Branded Reports <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li><i class="fa fa-check"></i> 500 Keywords <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li> <i class="fa fa-times"></i> 0 Social Account <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li> <i class="fa fa-times"></i>  Phone & Email Support <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                    </ul>
                </div>
             </div>
          </div>
        </div>
        <div class="col-md-4 ">
            <div class="pricing-table active">
             <div class="pricing-top">
               <div class="pricing-title">
                 <h3 class="mb-15">Standard</h3>
                 <p>Suitable for medium sit amet officia deserunt mollit anim id est laborum</p>
               </div>
               <div class="pricing-prize">
                 <h2>$99 /<span> month</span> </h2>
               </div>
               <div class="pricing-button">
                 <a class="button" href="#">Sign Up</a>
               </div>
             </div>
             <div class="pricing-content">
               <div class="pricing-table-list">
                    <ul class="list-unstyled">
                        <li> <i class="fa fa-check"></i> 15 Analytics Campaign <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span></li>
                        <li><i class="fa fa-check"></i> Branded Reports <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li><i class="fa fa-check"></i> 1200 Keywords <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li> <i class="fa fa-check"></i> 2 Social Account <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li> <i class="fa fa-times"></i>  Phone & Email Support <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                    </ul>
                </div>
             </div>
          </div> 
        </div>
        <div class="col-md-4 ">
           <div class="pricing-table">
             <div class="pricing-top">
               <div class="pricing-title">
                 <h3 class="mb-15">Premium</h3>
                 <p>Best for unlimited tempor incididunt ut labore et dolore magna aliqua</p>
               </div>
               <div class="pricing-prize">
                 <h2>$149 /<span> month</span> </h2>
               </div>
               <div class="pricing-button">
                 <a class="button" href="#">Sign Up</a>
               </div>
             </div>
             <div class="pricing-content">
               <div class="pricing-table-list">
                    <ul class="list-unstyled">
                        <li> <i class="fa fa-check"></i>25 Analytics Campaign <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span></li>
                        <li><i class="fa fa-check"></i> Branded Reports <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li><i class="fa fa-check"></i>1,900 Keywords <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li> <i class="fa fa-check"></i> 4 Social Account <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                        <li> <i class="fa fa-check"></i> Phone & Email Support <span class="tooltip-content float-right" data-placement="top" data-toggle="tooltip" data-original-title="Lorem ipsum dolor sit amet"><i class="fa fa-info"></i></span> </li>
                    </ul>
                </div>
             </div>
          </div> 
        </div>
    </div>
  </div>
</section>

<!--=================================
Price table -->

<!--=================================
 Our Testimonial -->

<section class="gray-bg page-section-pt happy-clients">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 align-self-end">
           <img class="d-xs-block d-lg-block d-none img-fluid" src="<?php PROOT ?>public/images/objects/testimonial.png" alt="">
      </div>
    <div class="col-lg-6 mt-60">
      <div class="section-title">
        <h6>Have a look at our</h6>
        <h2 class="title-effect">Client Opinions & Reviews</h2>
      </div>
     <div class="tab">
      <div class="tab-content" id="nav-tabContent">
       <div class="tab-pane fade show active" id="testi-01"> 
          <span class="quoter-icon">“</span>
          <p>I had a few things I needed help with on this template... Their customer service was amazing and helped me out many times. All fits and works well and good!! Top marks. One of the complete template with different requirements. Thanks a lot for such great features, pages, shortcodes and home variations. Incredible Job.</p>
          <div class="testimonial-avatar">
            <h5>Acapella </h5>
            <span>ThemeForest user</span>
          </div>
        </div>
        <div class="tab-pane fade" id="testi-02"> 
          <span class="quoter-icon">“</span>
          <p>Really like the cleanliness of the design, the documentation and the content-blocks. Obviously it is still a relatively new template (version 1.0.3), so it lacks some features that you'll find in more mature templates. But their support is swift and very co-operative. Kudos!</p>
          <div class="testimonial-avatar">
            <h5>Tenfore </h5>
            <span>ThemeForest user</span>
          </div>
        </div>
        <div class="tab-pane fade" id="testi-03">
          <span class="quoter-icon">“</span> 
          <p> One of the complete template with different requirements. Thanks a lot for such great features, pages, shortcodes and home variations. Incredible Job. I had a few things I needed help with on this template... Their customer service was amazing and helped me out many times. All fits and works well and good!! Top marks.</p>
          <div class="testimonial-avatar">
            <h5>Acapella </h5>
            <span>ThemeForest user</span>
          </div>
        </div>
        <div class="tab-pane fade" id="testi-04"> 
          <span class="quoter-icon">“</span>
          <p>The quality of design is very good and make sense to the real world requirement. yes its multipurpose template and i found what i wanted from this theme. perfectly suitable for my business and design is flexible with multiple layout provided. good work keep it up.</p>
          <div class="testimonial-avatar">
            <h5>Shopperbox </h5>
            <span>ThemeForest user</span>
          </div>
         </div>
         <ul class="nav nav-tabs mt-60" id="myTab" role="tablist">
           <li><a class="nav-item active" href="#testi-01" data-toggle="tab"><img class="img-fluid" src="<?php PROOT ?>public/images/team/01.jpg" alt=""> </a></li>
           <li><a class="nav-item" href="#testi-02" data-toggle="tab"><img class="img-fluid" src="<?php PROOT ?>public/images/team/02.jpg" alt=""> </a></li>
           <li><a class="nav-item" href="#testi-03" data-toggle="tab"><img class="img-fluid" src="<?php PROOT ?>public/images/team/03.jpg" alt=""> </a></li>
           <li><a class="nav-item" href="#testi-04" data-toggle="tab"><img class="img-fluid" src="<?php PROOT ?>public/images/team/04.jpg" alt=""> </a></li>
          </ul>
       </div>
     </div>
    </div>  
   </div>
 </div>
</section>     

<!--=================================
 Our Testimonial -->

<!--=================================
 Our Blog -->

<section class="our-blog page-section-ptb">
  <div class="container">
    <div class="row">
     <div class="col-md-12">
         <div class="section-title text-center">
            <h6>Fresh OnyxDS's</h6>
            <h2 class="title-effect">Latest Articles</h2>
          </div>
       </div>
    </div>
   <div class="row">
     <div class="col-md-4 xs-mb-30">
        <div class="blog-box  blog-1 h-100">        
         <div class="blog-info">
            <span class="post-category"><a href="#">Business</a></span>
            <h4> <a href="#"> Does your life lack meaning</a></h4>
            <p>I truly believe Augustine’s words are true and if you look at history you know it is true.</p>
            <span><i class="fa fa-user"></i> By Webster</span>
            <span><i class="fa fa-calendar-check-o"></i> 01 July 2018 </span>
            </div>  
            <div class="blog-box-img" style="background-image:url(images/blog/01.jpg);"></div>
          </div>
        </div>
     <div class="col-md-4 xs-mb-30">
        <div class="blog-box blog-1 h-100 active">        
         <div class="blog-info">
            <span class="post-category"><a href="#">Adventure</a></span>
            <h4> <a href="#">You will begin to realise</a></h4>          
            <p>We also know those epic stories, those modern-day legends surrounding the early.</p>
            <span><i class="fa fa-user"></i> By Admin</span>
            <span><i class="fa fa-calendar-check-o"></i> 21 June 2018 </span>
            </div>  
            <div class="blog-box-img" style="background-image:url(images/blog/09.jpg);"></div>
        </div>
        </div>
     <div class="col-md-4">
        <div class="blog-box blog-1 h-100">
            <div class="blog-info">
             <span class="post-category"><a href="#">Travel</a></span>
              <h4> <a href="#"> Step out on to the path</a></h4>         
              <p>I truly believe Augustine’s words are true and if you look at history you know it is true.</p>
              <span><i class="fa fa-user"></i> By User</span>
              <span><i class="fa fa-calendar-check-o"></i> 13 may 2018 </span>
              </div>
            <div class="blog-box-img" style="background-image:url(images/blog/03.jpg);"></div>
          </div>
       </div>
     </div>
    </div>
</section>

<!--=================================
 Our Blog -->

<!--=================================
raindrops -->

<section id="raindrops" class="raindrops" style="height: 50px;"></section>

<!--=================================
raindrops  -->

<!--=================================
 action-box -->

<section class="contact-box contact-box-top theme-bg">
  <div class="container">
    <div class="row pt-20 pb-40">
      <div class="col-md-4 sm-mb-30">
        <div class="contact-box">
          <div class="contact-icon">
            <i class="ti-map text-white"></i>
          </div>
          <div class="contact-info">
            <h5 class="text-white"> 66 Alimosho Rd., Alaguntan</h5>
            <span class="text-white">Iyana Ipaja, Lagos, Nigeria.</span>
          </div>
        </div>
      </div>
      <div class="col-md-4 sm-mb-30">
        <div class="contact-box">
          <div class="contact-icon">
            <i class="ti-headphone text-white"></i>
          </div>
          <div class="contact-info">
            <h5 class="text-white">+(234) 802-725 7478</h5>
            <span class="text-white">Mon-Fri 8:30am-5:00pm</span>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="contact-box">
          <div class="contact-icon">
            <i class="ti-email text-white"></i>
          </div>
          <div class="contact-info">
            <h5 class="text-white">letstalk@onyxdatasystems.com</h5>
          <span class="text-white">24 X 7 online support</span>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--=================================
 action-box -->

<!--=================================
google-map -->
  
 <section class="google-map black-bg">
   <div class="container">
     <div class="row">
       <div class="col-lg-12">
          <div class="map-icon">
          </div>
       </div>
     </div>
   </div>
   <div class="map-open">
     <div style="width: 100%; height: 300px;" id="map" class="g-map" data-type='black'></div>
   </div>
 </section>

<!--=================================
google-map -->

<?php $this->end() ?>
