<?php $this->start('body') ?>

<section class="page-title bg-overlay-black-60 parallax" data-jarallax='{"speed": 0.6}' 
          style="background-image: url(<?php echo PROOT ?>public/images/bg/03.jpg); height: 250px;">
  <div class="container">
    <div class="row"> 
      <div class="col-lg-12"> 
      <div class="page-title-name">
          <h1>About The Company</h1>
          <p>We are a force to reckon with in the Software Industry</p>
        </div>
     </div>
   </div>
  </div>
</section>
<!--=================================
action box- -->

<section class="action-box theme-bg full-width">
  <div class="container">
    <div class="row">
     <div class="col-lg-12 col-md-12">
       <div class="action-box-text">
        <h3><strong> Webster: </strong> The most powerful template ever on the market</h3>
        <p>Create tailor-cut websites with the exclusive multi-purpose responsive template along with powerful features.</p>
      </div>
      <div class="action-box-button">
        <a class="button button-border white" href="#">
          <span>Purchase Now</span>
          <i class="fa fa-download"></i>
       </a> 
     </div>
    </div>
  </div>
 </div>
</section>
 
<!--=================================
action box- -->
 
 
<?php $this->end() ?>