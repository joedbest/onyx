<?php 

    define('DEBUG', true); //controls error reporting

    define('DBHOST', '127.0.0.1');
    define('DBNAME', 'onyx');
    define('DBUSER', 'root');
    define('DBPASSWORD', '');
    
    define('DEFAULT_CONTROLLER', 'Home'); //default controller if no controller is set
    define('DEFAULT_LAYOUT', 'default'); //default layout if no layout is specified

    define('PROOT', '/onyx/'); //set this to '/' during production

    define('SITE_TITLE', 'Home'); //the default site title if the site title is not set

    define('STORE_COOKIE_NAME', 'kdf939u83lkdkdl23jlsf3f7ys');
    define('USER_COOKIE_EXPIRY', time() + (86400 * 30));

?>